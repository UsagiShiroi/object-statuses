<?php

namespace Status\Exception;

use Exception;

/**
 * Class ChangeRestrictedException
 *
 * @package Status\Exception
 */
class ChangeRestrictedException extends Exception
{
}
