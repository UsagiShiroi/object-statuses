<?php

namespace Status\Exception;

use Exception;

/**
 * Class StatusOwnerException
 *
 * @package Status\Exception
 */
class StatusOwnerException extends Exception
{
}
