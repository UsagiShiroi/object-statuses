<?php

namespace Status\Exception;

use Exception;

/**
 * Class UndefinedStatusFieldException
 *
 * @package Status\Exception
 */
class UndefinedStatusFieldException extends Exception
{
}
