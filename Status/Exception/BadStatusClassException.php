<?php

namespace Status\Exception;

use Exception;

/**
 * Class BadStatusClassException
 *
 * @package Status\Exception
 */
class BadStatusClassException extends Exception
{
}
