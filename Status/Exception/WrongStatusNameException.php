<?php

namespace Status\Exception;

use Exception;

/**
 * Class WrongStatusNameException
 *
 * @package Status\Exception
 */
class WrongStatusNameException extends Exception
{
}
