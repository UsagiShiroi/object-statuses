<?php

namespace Status\Exception;

use Exception;

/**
 * Class EmptyStatusRemoveException
 *
 * @package Status\Exception
 */
class EmptyStatusRemoveException extends Exception
{
    /**
     * Constructor.
     *
     * @param integer $code error code
     * @param Exception $previous [optional] previous exception (default=null)
     */
    public function __construct($code = 0, Exception $previous = null)
    {
        parent::__construct('Невозможно удалить "пустой" статус', $code, $previous);
    }
}
