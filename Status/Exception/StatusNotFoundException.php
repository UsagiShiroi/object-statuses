<?php

namespace Status\Exception;

use Exception;

/**
 * Class StatusNotFoundException
 *
 * @package Status\Exception
 */
class StatusNotFoundException extends Exception
{
}
