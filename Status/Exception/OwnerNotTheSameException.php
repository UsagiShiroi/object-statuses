<?php

namespace Status\Exception;

use Exception;

/**
 * Class OwnerNotTheSameException
 *
 * @package Status\Exception
 */
class OwnerNotTheSameException extends Exception
{
}
