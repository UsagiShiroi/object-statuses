<?php

namespace Halk\Module\Status\Test;

use Status\Model\NoStatus;
use Status\Model\StatusData;
use PHPUnit_Framework_TestCase;

/**
 * Class NoStatusTest
 *
 * @package Halk\Module\Status\Test
 */
class NoStatusTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TestOwner
     */
    protected $status_owner;
    /**
     * @var NoStatus
     */
    protected $empty_status;

    public function setUp()
    {
        $this->status_owner = new TestOwner();
        $this->empty_status = new NoStatus(new StatusData(), $this->status_owner);
    }

    public function testSet()
    {
        $this->status_owner->status_id = 123;
        (new NoStatus(new StatusData(), $this->status_owner))->set();
        $this->assertEquals(null, $this->status_owner->status_id);
    }

    /**
     * @expectedException \Halk\Module\Status\Exception\EmptyStatusRemoveException
     */
    public function testRemove()
    {
        (new NoStatus(new StatusData(), $this->status_owner))->remove();
    }
}
