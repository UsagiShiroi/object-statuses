<?php

namespace Halk\Module\Status\Test;

use Status\Model\StatusData;
use Status\Model\StatusFactory;
use PHPUnit_Framework_TestCase;

/**
 * Class StatusfactoryTest
 *
 * @package Halk\Module\Status\Test
 */
class StatusFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var StatusFactory
     */
    public $factory;

    /**
     * @var TestOwner
     */
    public $owner;
    /**
     * @var StatusData
     */
    public $status_data;

    public function setUp()
    {
        $this->factory = $this->getMockBuilder('\Status\Model\StatusFactory')
            ->setMethods(['getStatusPath'])
            ->getMock();
        $this->factory->expects($this->any())
            ->method('getStatusPath')
            ->will($this->returnValue('\Halk\Module\Status\Test\\'));
        $this->owner = new TestOwner();
        $this->status_data = new StatusData(['model_id' => $this->owner->model_id]);
    }

    public function testCreateStatus()
    {
        $this->status_data->system_name = 'test_status';
        $status = $this->factory->createStatus($this->status_data, $this->owner);
        $this->assertTrue($status instanceof Status);
    }

    /**
     * @expectedException \Status\Exception\WrongStatusNameException
     */
    public function testBadSystemName()
    {
        $this->status_data->system_name = 'test';
        (new StatusFactory)->createStatus($this->status_data, new TestOwner());
    }

    /**
     * @expectedException \Status\Exception\StatusNotFoundException
     */
    public function testStatusClassNotFound()
    {
        $this->status_data->system_name = 'test_this_class_does_not_exist';
        (new StatusFactory)->createStatus($this->status_data, new TestOwner());
    }
}
