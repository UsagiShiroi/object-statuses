<?php

namespace Status\Test;

use Status\Model\IModel;
use Status\Model\StatusTrait;

/**
 * Class TestOwner
 *
 * @package Status\Test
 */
class TestOwner implements IModel
{
    use StatusTrait;
    /**
     * @var int
     */
    public $model_id = 123;
    /**
     * @var int
     */
    public $status_id = 123;

    /**
     * @return int
     */
    public function getModelId()
    {
        return $this->model_id;
    }
}
