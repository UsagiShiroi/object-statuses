<?php

namespace Halk\Module\Status\Test;

use Status\Model\StatusAbstract;
use Status\Model\StatusData;
use PHPUnit_Framework_TestCase;

/**
 * Class StatusAbstractTest
 *
 * @package Halk\Module\Status\Test
 */
class StatusAbstractTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var StatusAbstract
     */
    public $status;

    /**
     * @var StatusAbstract
     */
    public $new_status;

    /**
     * @var TestOwner
     */
    public $owner;

    public function setUp()
    {
        $this->owner = new TestOwner();
        $this->new_status = new Status(new StatusData(['model_id' => $this->owner->model_id]), $this->owner);
        $this->new_status->status_data->id = 321;
        $this->status = $this->getMockBuilder('\Status\Model\StatusAbstract')
            ->setConstructorArgs(
                array(
                    new StatusData(['model_id' => $this->owner->model_id]),
                    $this->owner
                )
            )
            ->setMethods(['getNewStatus', 'canChangeTo'])
            ->getMockForAbstractClass();
        $this->status->expects($this->any())->method('getNewStatus')->will($this->returnValue($this->new_status));
        $this->status->status_data->id = $this->owner->status_id;
    }

    public function testConstructor()
    {
        $this->status->__construct($this->status->status_data, $this->owner);
        $this->assertTrue(true);
    }

    /**
     * @expectedException \Status\Exception\StatusOwnerException
     */
    public function testWrongOwnerModelId()
    {
        $this->status->status_data->model_id = $this->owner->model_id + 3;
        $this->status->__construct($this->status->status_data, $this->owner);
    }

    public function testChange()
    {
        $this->status->expects($this->any())
            ->method('canChangeTo')
            ->will($this->returnValue([$this->new_status->status_data->id]));
        $this->status->change($this->new_status->status_data);
        $this->assertEquals($this->new_status->status_data->id, $this->owner->status_id);
    }

    /**
     * @expectedException \Status\Exception\ChangeRestrictedException
     */
    public function testChangeRestricted()
    {
        $this->status->change(new StatusData());
    }

    public function testSet()
    {
        $this->status->status_data->id = $this->owner->status_id + 3;
        $this->status->set();
        $this->assertEquals($this->status->status_data->id, $this->owner->status_id);
    }

    public function testRemove()
    {
        $this->status->remove();
        $this->assertEquals(null, $this->owner->status_id);
    }
}
