<?php

namespace Status\Model;

use Exception;
use Halk\Module\Status\Exception\EmptyStatusRemoveException;

/**
 * Class NoStatus
 * Класс, который возвращается, когда статус не указан;
 * @package Status\Model
 */
class NoStatus extends StatusAbstract
{
    /**
     * @throws EmptyStatusRemoveException
     */
    public function remove()
    {
        throw new EmptyStatusRemoveException();
    }

    /**
     * @param array $context
     * @param StatusData $new_status_data
     * @return void
     */
    public function change(StatusData $new_status_data, array $context = [])
    {
        $new_status = (new StatusFactory)->createStatus($new_status_data, $this->status_owner);
        $new_status->set($context);
    }

    /**
     * @param array $context
     * @throws Exception
     */
    public function set(array $context = [])
    {
        $this->status_owner->{$this->status_owner->status_field} = null;
    }

    /**
     * Проверка на то, что владелец указан верно
     *
     * @param $status_owner - обладатель статуса
     */
    protected function validateOwner($status_owner)
    {
    }
}
