<?php

namespace Status\Model;

use Halk\Module\Status\Exception\ChangeRestrictedException;
use Halk\Module\Status\Exception\OwnerNotTheSameException;
use Halk\Module\Status\Exception\StatusOwnerException;
use Halk\Module\Status\Exception\UndefinedStatusFieldException;

/**
 * Class StatusAbstract
 *
 * @package Status\Model
 * @abstract
 */
abstract class StatusAbstract implements IStatus
{
    /**
     * @var StatusData $status_data
     */
    public $status_data;
    /**
     * @var object
     */
    public $status_owner;

    /**
     * StatusAbstract constructor.
     * Храним данные статуса.
     * @param StatusData $status_data
     * @param IModel $status_owner - объект(в смысле объект класса), для которого создаем статус
     * @throws StatusOwnerException
     * @throws UndefinedStatusFieldException
     */
    public function __construct(StatusData $status_data, IModel $status_owner)
    {
        $this->status_data = $status_data;
        $this->validateOwner($status_owner);
        $this->status_owner = $status_owner;
    }

    /**
     * Смена статуса
     *
     * @param StatusData $new_status_data - данные нового статуса
     * @param array $context - дополнительные данные, которые мы бы хотели использовать при выставлении нового статуса
     * @return mixed
     * @throws ChangeRestrictedException
     */
    public function change(StatusData $new_status_data, array $context = [])
    {
        if ($this->status_data->id !== $new_status_data->id) {
            $new_status = $this->getNewStatus($new_status_data);
            $this->validateChange($new_status);
            $this->remove();
            $new_status->set($context);
        }
    }

    /**
     * Установка статуса
     * @param array $context - Дополнительные данные
     */
    public function set(array $context = [])
    {
        $this->status_owner->{$this->status_owner->status_field} = $this->status_data->id;

    }

    /**
     * Удаление статуса
     */
    public function remove()
    {
        (new NoStatus(new StatusData(), $this->status_owner))->set();
    }

    /**
     * Проверка на то, что смена статуса допустима
     * @param StatusAbstract $new_status
     * @throws ChangeRestrictedException
     * @throws OwnerNotTheSameException
     */
    protected function validateChange($new_status)
    {
        if (!in_array($new_status->status_data->id, $this->canChangeTo())) {
            throw new ChangeRestrictedException('Запрещено изменять текущий статус на указанный');
        }
        if ($new_status->status_owner !== $this->status_owner) {
            throw new OwnerNotTheSameException('Владелец нового статуса отличается от владельца текущего');
        }
    }

    /**
     * Проверка на то, что владелец указан верно
     * @param IModel $status_owner - обладатель статуса
     * @throws UndefinedStatusFieldException
     * @throws StatusOwnerException
     */
    protected function validateOwner(IModel $status_owner)
    {
        $owner_model_id = $status_owner->getModelId();
        if ($owner_model_id !== $this->status_data->model_id) {
            throw new StatusOwnerException('Нельзя использовать статус не принадлежащий этой модели');
        }
        if (!property_exists($status_owner, 'status_field')
            || !is_string($status_owner->status_field)
            || !$status_owner->status_field
        ) {
            throw new UndefinedStatusFieldException('У класса должно быть задано свойство status_field');
        }
    }

    /**
     * @param StatusData $new_status_data
     * @return StatusAbstract
     * @throws \Halk\Module\Status\Exception\BadStatusClassException
     * @throws \Halk\Module\Status\Exception\StatusNotFoundException
     * @throws \Halk\Module\Status\Exception\WrongStatusNameException
     */
    protected function getNewStatus(StatusData $new_status_data)
    {
        return (new StatusFactory)->createStatus($new_status_data, $this->status_owner);
    }

    /**
     * Возвращает массив id статусов, на которые можно сменить этот статус
     * Для этого делается выборка по модели StatusMap где from_id - это id текущего статуса
     * @return array
     */
    abstract public function canChangeTo();
}
