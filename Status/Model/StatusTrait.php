<?php

namespace Status\Model;

use Halk\Module\Status\Exception\StatusNotFoundException;
use Exception;

trait StatusTrait
{
    public $status_field = 'status_id';


    /**
     * Смена статуса
     * @param StatusData $new_status_data - данные нового статуса
     * @param $context - данные, которые мы хотим передать
     * @throws Exception
     */
    public function changeStatus(StatusData $new_status_data, array $context = [])
    {
        $current_status = $this->getCurrentStatus();
        $current_status->change($new_status_data, $context);
    }

    /**
     * Возвращает текущий статус
     * @return StatusAbstract
     * @throws Exception
     */
    public function getCurrentStatus()
    {
        $status_data = $this->getStatusData();
        if (is_null($status_data)) {
            return new NoStatus(new StatusData(), $this);
        }
        return (new StatusFactory)->createStatus($this->getStatusData(), $this);
    }

    /**
     * Возвращет статус по его системному имени
     * @param $system_name
     * @return StatusAbstract
     * @throws StatusNotFoundException
     */
    public function getStatusBySystemName($system_name)
    {
        $status_data = $this->getStatusDataBySystemName($system_name);
        return (new StatusFactory)->createStatus($status_data, $this);
    }

    /**
     * Удаляет текущий статус
     */
    public function removeStatus()
    {
        $this->getCurrentStatus()->remove();
    }

    /**
     * Возвращает данные о текущем статусе. Делает выборку из StatusData по значению, содержащемуся в поле, название
     * которого указано в $this->status_field. В случае если такой статус не найден вернет null
     * @return StatusData|null
     */
    abstract public function getStatusData();

    /**
     * Возвращает данные статуса по его system_name. Должен делать выборку из модели StatusData по system_name
     * @param $system_name
     * @return StatusData
     * @throws StatusNotFoundException
     */
    abstract public function getStatusDataBySystemName($system_name);

    /**
     * Возвращает все StatusData для объекта, выбирая их по model_id
     *
     * @abstract
     * @param bool $as_array
     * @return StatusData[]
     */
    abstract public function statusDataList($as_array = false);

    /**
     * Должен возвращать model_id объекта
     * @return int
     */
    abstract public function getModelId();
}
