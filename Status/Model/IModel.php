<?php

namespace Status\Model;

/**
 * Interface IModel
 *
 * @package Status\Model
 */
interface IModel
{
    /**
     * Должен вернуть model_id
     * @return int
     */
    public function getModelId();
}
