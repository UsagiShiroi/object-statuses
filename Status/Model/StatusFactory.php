<?php

namespace Status\Model;

use Halk\Module\Status\Exception\BadStatusClassException;
use Halk\Module\Status\Exception\StatusNotFoundException;
use Halk\Module\Status\Exception\WrongStatusNameException;
use ReflectionClass;

/**
 * Class StatusFactory
 * Фабрика по созданию экземпляров статусов.
 * @package Status\Model
 */
class StatusFactory
{
    const MODULES_PATH = 'Statuses';

    protected $status_path;

    /**
     * Разбираем system_name на составные части, разделенные "_", их должно быть не меньше 2х.
     * {module_name}_{any_number_of_folders_name}_{status_name}
     * Сначала название модуля, потом люое количество папок внутри папки Statuses в этом модуле.
     * Последняя часть - имя класса статуса.
     * @param StatusData $status_data
     * @param IModel $status_owner - объект(в смысле объект класса), для которого создаем статус
     * @returns StatusAbstract
     * @throws StatusNotFoundException
     * @throws WrongStatusNameException
     * @throws BadStatusClassException
     */
    public function createStatus(StatusData $status_data, IModel $status_owner)
    {
        $name_parts = explode('_', $status_data->system_name);
        if (count($name_parts) < 2) {
            throw new WrongStatusNameException(
                'system_name должен иметь хотя бы две части, разделенные "_", текущее значение: '
                . $status_data->system_name
            );
        }
        $module_name = ucfirst(array_shift($name_parts));
        $status_class = ucfirst(array_pop($name_parts));
        $class_name = $this->getStatusPath($module_name);
        while (count($name_parts) > 0) {
            $class_name .= ucfirst(array_shift($name_parts)) . '\\';
        }
        $class_name .= $status_class;
        if (!class_exists($class_name)) {
            throw new StatusNotFoundException('Класс ' . $class_name . ' не существует');
        }
        if (!array_search(StatusAbstract::class, class_parents($class_name))) {
            throw new BadStatusClassException('Класс ' . $class_name . ' должен наследовать ' . StatusAbstract::class);
        }
        $constructor_reflection = (new ReflectionClass($class_name))->getConstructor();
        if (!$constructor_reflection->isPublic()) {
            throw new BadStatusClassException('Конструктор класса должен быть public');
        }
        return new $class_name($status_data, $status_owner);
    }

    /**
     * @param $status_path
     */
    protected function setStatusPath($status_path)
    {
        $this->status_path = $status_path;
    }

    /**
     * @param $module_name
     * @return string
     */
    protected function getStatusPath($module_name)
    {
        if ($this->status_path) {
            return $this->status_path;
        }
        return self::MODULES_PATH . '\\' . $module_name . '\\Statuses\\';
    }
}
