<?php

namespace Status\Model;

/**
 * Interface IStatus
 *
 * @package Status\Model
 */
interface IStatus
{
    /**
     * Смена статуса
     * @param StatusData $new_status_data - статус, на которые изменяем
     * @param array $context - дополнительные данные, которые мы бы хотели использовать при выставлении нового статуса
     * @return mixed
     */
    public function change(StatusData $new_status_data, array $context = []);

    /**
     * Удаляет статус
     */
    public function remove();

    /**
     * Устанавливает нвоый статус
     * @param array $context - Дополнительные данные
     */
    public function set(array $context = []);
}
