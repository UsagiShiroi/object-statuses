<?php

namespace Status\Model;

/**
 * Class StatusMap
 * Разрешенные изменения статусов
 * @package Status\Model
 * @property int $id - primary key, просто айдишник
 * @property int $from_id - id статуса, который изменяем
 * @property int $to_id - id статуса, на который изменяем
 */
class StatusMap
{
    /**
     * Различные реализации модели, в зависимости от используемой ORM
     */
}
