<?php

namespace  Status\Model;

/**
 * Class StatusData
 *
 * @package Status\Model
 * @property int $id - id
 * @property string $system_name - системное имя, используется в фабрике для определения класса
 * @property string $description - Описание статуса, человекочитаемое название статуса
 * @property int $model_id - айди модели, для которой используется этот статус
 * @property bool $closed - является-ли данный статус "закрывающим"
 * @property string $data - дополнительные данные
 */
class StatusData
{
    /**
     * Различные реализации модели, в зависимости от используемой ORM
     */
}
