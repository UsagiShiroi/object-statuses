### Как добавить новые статусы

Для использования статусов для объектов какого-либо класса
необходимо выполнить следующие действия:
1. Убедиться, что класс - является моделью, если нет,
 то добавить его в модели, класс должен наследовать интерфейс **IModel**
2. Добавить данные новых статусы в БД. При добавлении необходимо учесть,
что system_name формируется следующим образом: `{module_name}_{any_number_of_folders_name}_{status_name}`
Сначала название модуля, потом любое количество папок внутри папки Statuses в этом модуле.
3. В соответствии с заданными system_name создаем классы наших статусов,
 которые должны иметь в родителях `Status\Model\StatusAbstract` .
 Например: для `sytem_name = deals_active' необходимо создать класс
 `Deals\Statuses\Active`, а для `system_name = deals_bc_folder_deleted`
 класс `Deals\Bc\Folder\Deleted`
4. Используем трейт `StatusTrait`  в классе. Если использовать его
нет желания, то необходимо создать свойство `$status_field`, в котором
будет содержаться название поля, в котором будет храниться id статуса.

В случае необходимости в данных классах можно задать собственную логику,
описав ее в методах `set()`, `remove()` и `change()`

По умолчанию при смене класса с помощью метода `change()` происоходит
следующее:
1. Вызывается `remove()` у статуса, который меняем
2. Вызывается `set()` у статуса, на который меняем

### Описание классов
Описание имеющихся классов
##### IStatus
Интерфейс, в котором определены 3 основных метода, которые
должен иметь каждый статус.
##### NoStatus
Пустой статус, который возвращается для тех случаев, когда статус
объекта не задан
##### StatusAbstract
Класс, от которого должны быть унаследованы все осатльные статусы.
Содержит всю общую логику статусов.
##### StatusData
Модель, в которой хранятся данные статусов
##### StatusMap
Модель, в которой хранятся все доступные переходы из одного
статуса в другой
##### StatusFactory
Фабрика для создания эксхземпляров статусов по их данным
##### StatusTrait
Трейт, содержащий в себе вспомогательные методы для работы со статусами
из класса, который их использует.

### Принцип работы
Каждый статус - это отдельный класс, неймспейс которого определяет его
`system_name`

Данные каждого статуса хранятся в модели `StatusData`.

Каждый статус имеет `model_id`, который определяет с объектами каких
классов может использоваться данный статус

Каждый статус имеет список статусов, на которые он может быть изменен.
По умолчанию доступные изменения хранятся в модели `StatusMap`

У каждого статуса публично доступны как минимум 4 метода:
* `set` - устанавливает статус
* `change` - осуществляет смену статуса на указанный
* `remove` - удаляет статус
* `canChangeTo` - возвращает список id статусов, на который можно изменить
 этот статус

Для создания экземпляра класса в его конструктор необходимо передать 2
аргумента:
* `$status_data` - данные статуса, экземпляр класса `StatusData`
* `$status_owner` - объект - владелец статуса, должен реализовывать
интерфейс `IModel`

Каждый класс хранит в себе свои данные (свойство `$status data`) и того,
кто является его владельцем (свойство `$status_owner`)

Для удобство создания экземпляра статуса сделана фабрика `StatusFactory`
которая имеет метод `createStatus`